\input{src/macros.tex}

\title{Autour de l'opérateur laplacien et les différences finies}
\author{Marc \textsc{Chevalier}}
\date{\today}

\begin{document}

\maketitle
\tableofcontents

\section[Valeurs propres du laplacien dans un domaine polygonal]{Valeurs propres du laplacien dans un domaine polygonal \---- le problème de Dirichlet}

\subsection{Cas d'un domaine carré}

\subsubsection*{Tâche 1}

On suppose qu'il existe une solution $u$ du problème de \textsc{Dirichlet} à variables séparées et on pose $u(x,y) = f(x)g(y)$.

\[
    \begin{aligned}
        \Delta u(x,y) &= \frac{\partial^2 u}{\partial x^2}(x,y) + \frac{\partial^2 u}{\partial y^2}(x,y)\\
        &= g(y)\frac{\dd^2 f}{\dd x^2}(x) + f(x)\frac{\dd^2 g}{\dd y^2}(y)\\
    \end{aligned}
\]
donc en injectant dans l'équation
\begin{equation}\label{1:1}
    \begin{aligned}
        -\Delta u(x,y) = \lambda u &\LRa g(y)f''(x) + f(x)g''(y) = \lambda f(x)g(y) \\
        &\LRa g(y)f''(x)+(g''(y)+\lambda g(y))f(x)=0
    \end{aligned}
\end{equation}
d'où, il existe $k_f\in \CC$ tel que
\[
    f'' + k_f^2 f = 0
\]
De même, en reprenant \ref{1:1} mais en factorisant par $g(y)$, on trouve qu'il existe $k_g\in \CC$ tel que
\[
    g'' + k_g^2 g = 0
\]

On a donc $f$ de la forme $C_f \sin(k_f x + \varphi_f)$ et $g$ de la forme $C_g \sin(k_g x + \varphi_g)$.

Maintenant, on utilise les contraintes sur $f$ pour déterminer $k_f$ et $\varphi_f$. En effet, on sait qu'on a $f(-1) = 0$ et $f(1) = 0$. Donc $k_f$ est de la forme $\frac{m\pi}{2}$ avec $m\in\NN$ et $\varphi_f = k_f$.

Aussi, $f$ est de la forme $x\mapsto C_f \sin\left(\frac{m\pi}{2}(x+1)\right)$ et $g$, par le même raisonnement est de la forme $y \mapsto C_g \sin\left(\frac{n\pi}{2}(y+1)\right)$.

Par conséquent, $u$ est de la forme $(x,y) \mapsto C \sin\left(\frac{m\pi}{2}(x+1)\right) \sin\left(\frac{n\pi}{2}(y+1)\right)$

On vérifie réciproquement que ces fonctions sont solutions.

On pose $u(x,y) = C \sin\left(\frac{m\pi}{2}(x+1)\right) \sin\left(\frac{n\pi}{2}(y+1)\right)$ avec $(m,n) \in \NN^2$ et $C\in\RR$.

\[
    \begin{aligned}
        \Delta u(x,y) &= -\frac{(m^2+n^2)\pi^2}{4} C \sin\left(\frac{m\pi}{2}(x+1)\right) \sin\left(\frac{n\pi}{2}(y+1)\right) \\
        &= -\frac{(m^2+n^2)\pi^2}{4} u(x,y)
    \end{aligned}
\]

Donc $u$ est bien vecteur propre du laplacien avec les conditions de \textsc{Dirichlet} de valeur propre $\frac{(m^2+n^2)\pi^2}{4}$.

D'autre part, comme on veut $u \neq 0$, on impose que $m^2 + n^2 \neq 0$, autrement dit $(m,n) \neq 0$.

\subsubsection*{Tâche 2}

En nommant $B\in\M_{N-1}(\ZZ)$ la matrice
\[
    B = \left(
        \begin{matrix}
            4 & -1 \\
            -1 & 4 & -1 \\
            & -1 & 4 & \ddots\\
            & & \ddots & \ddots & -1\\
            & & & -1 & 4
        \end{matrix}
    \right)
\]

on a

\[
    A_h = \frac{1}{h^2} \left(
        \begin{matrix}
            B & -I_{N-1} \\
            -I_{N-1} & B & -I_{N-1} \\
            & -I_{N-1} & B & \ddots\\
            & & \ddots & \ddots & -I_{N-1}\\
            & & & -I_{N-1} & B
        \end{matrix}
    \right)\in\M_{(N-1)^2}(\QQ)
\]
en prenant en compte les <<~effets de bord~>>.

Toute fois, c'est pas très pratique, et surtout pas adapté à une manipulation d'une matrice creuse.

On utilise les vecteurs de voisinage \texttt{nord}, \texttt{sud}, \texttt{ouest} et \texttt{est} pour déduire les coordonnées des $-1$ dans la matrice $A_h$. Ainsi, on utile \texttt{sparse(i,j,v,m,n)}\footnote{\url{http://fr.mathworks.com/help/matlab/ref/sparse.html?searchHighlight=sparse}}.

\subsubsection*{Tâche 3}

Les valeurs numériques des valeurs propres exactes sont, à 10 décimales 
\[
    \begin{aligned}
        4&.9348022005 \\
        12&.3370055014 \\
        12&.3370055014 \\
        19&.7392088022 \\
        24&.6740110027 \\
        24&.6740110027 \\
        32&.0762143035 \\
        32&.0762143035 \\
        41&.9458187046 \\
        41&.9458187046
    \end{aligned}
\]
et les valeurs propres approximées avec $N=1600$ 
\[
    \begin{aligned}
        4&.9348006131\\
        12&.3369920083\\
        12&.3369920083\\
        19&.7391834035\\
        24&.6739459186\\
        24&.6739459186\\
        32&.0761373138\\
        32&.0761373138\\
        41&.9456147218\\
        41&.9456147218
    \end{aligned}
\]
avec les erreurs relatives
\[
    \begin{aligned}
        0&.0000003217\\
        0&.0000010937\\
        0&.0000010937\\
        0&.0000012867\\
        0&.0000026378\\
        0&.0000026378\\
        0&.0000024002\\
        0&.0000024002\\
        0&.0000048630\\
        0&.0000048630
    \end{aligned}
\]

La calcul avec $N=1000$ prend 33.702s sur un Intel Core i7 (Ivy Bridge) 3610QM. En particulier, on constante que 32s est passé dans le calcul de \texttt{eigs} en utilisant le code profiler de Matlab.

Les matrices sont suffisamment grandes pour avoir un gain de mémoire en utilisant des matrices creuses mais aussi des gains de performances liés aux algorithmes spécifiques aux matrices creuses et à une meilleur localité en mémoire.

Toutefois, pour observer ces gains, il faut utiliser de plus petites valeurs de N pour s'abstraire successivement de la taille en mémoire puis des gains liés à la localité.

Le Core i7 3610QM possède un cache L3 de 6 Mo et permet donc de stocker 750 000 flottants double précision (64 bits) soit une matrice de taille de plus de 800 de côté, soit environ $N=28$. Aussi, les gains, au moins au niveau de la localité devrait s'observer jusqu'à cette limite, ce qui est bien faible...


\bigskip

Le choix de $N=1000$ est totalement expérimental. Par une recherche manuelle, on trouve que le temps de calcul pour $N=1000$ est encore acceptable et la marge d'erreur est de l'ordre de $10^{-6}$, ce qui est largement acceptable pour se rendre compte que la solution approximée converge bien vers un échantillonnage de la solution exacte.

Certains des calculs représentés par la suite ont été fait avec $N=1600$ mais cela demande plusieurs dizaines de minutes de calcul et la contribution d'un important swap pour avoir une mémoire vive disponible totale de plusieurs dizaines de Go.

D'autre part, une expérience malheureuse montre que le calcul pour $N=2000$ n'est pas envisageable.

\begin{figure}[!ht]
    \centering
    \subfloat[$\lambda = 2\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp2}}
    \subfloat[$\lambda = 5\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp5}}\\
    \subfloat[$\lambda = 5\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp5_}}
    \subfloat[$\lambda = 8\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp8}}\\
    \subfloat[$\lambda = 10\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp10}}
    \subfloat[$\lambda = 10\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp10_}}
    \caption{Solutions pour différents modes}
\end{figure}

\FloatBarrier

On observe une forme de dualité assez prévisible entre les solutions qui partagent la même valeur propre.

\begin{figure}[!ht]
    \centering
    \subfloat[$\lambda = 13\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp13}}
    \subfloat[$\lambda = 13\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp13_}}\\
    \subfloat[$\lambda = 17\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp17}}
    \subfloat[$\lambda = 17\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk3vp17_}}
    \caption{Solutions pour différents modes}
\end{figure}

\subsection{Cas d'une domaine en forme de L}
\subsubsection*{Tâche 4}

Recollement des solutions particulières dans chaque carré. On sait qu'elles se recollent naturellement en valeur grâce à la condition de Dirichlet. Il suffit de choisir celles qui se recolent également en dérivée.

\[
    u(x,y) = C \sin\left(m\pi(x+x_0)\right) \sin\left(n\pi(y+y_0)\right)
\]
est une forme générale de solution pour un carré de côté 1. $x_0$ et $y_0$ sont des constantes qui dépendent de la position du carré dans le plan. Dans notre cas, on a 
\[
    (x_0,y_0) \in \set{(-1,-1),(-1,0),(0,-1)}{}
\]

\[
    \frac{\partial u}{\partial x}u(x,y) = C m\pi\cos\left(m\pi(x+x_0)\right) \sin\left(n\pi(y+y_0)\right)
\]
et
\[
    \frac{\partial u}{\partial x}u(x,y) = C n\pi\sin\left(m\pi(x+x_0)\right) \cos\left(n\pi(y+y_0)\right)
\]

Ainsi, on peut calculer la dérivée première sur les bords. On observe qu'en générale, elle est non nulle.

Par conséquent, une famille de fonctions solutions particulière est constituée d'une fonction de la forme précédemment vu sur le carré $]-1;0[\times]-1;0[$ et de son opposée sur les carrées $]0;1[\times]-1;0[$ et $]-1;0[\times]0;1[$.

La valeur propre associée à ce genre de fonctions est $(m^2+n^2)\pi^2$, ce qui est prévisible car on a doublé la fréquence d'une fonction qui est dérivée 2 fois.

D'autre part, on peut imaginer une autre classe de fonctions. Celles ci ressembleraient à celles d'un domaine carré mais étendues sur un rectangle de longueur 2 et de largeur 1, le carré laissé reçoit 0. On aurait alors les valeurs propres de la forme $m^2\pi^2 + \frac{n^2\pi^2}{4}$.

\subsubsection*{Tâche 5}

On choisit une numérotation dans le même esprit de la précédente. On numérote chaque état, dans l'ordre croissant sur chaque ligne, en allant vers le nord et en parcourant les colonnes d'ouest en est. On a ainsi une rupture de régularité en passant à la limite dont il faut tenir compte. C'est un peu fin, mais ça marche. Le code qui fait ça n'a strictement aucun intérêt autre que d'avoir été écris pour tout faire sans condition explicite.

\subsubsection*{Tâche 6}

On trouve des fonctions propres associées à des valeurs propres prévues à la tâche 4 : des produits de $\pi^2$ par une somme de carrés (ie. une somme de carrés de multiples de $\pi$). Ces fonctions ont une allure attendue avec des comportements similaires dans chaque sous carré et une annulation sur les bords de ceux-ci. Les autres ont bien un motif qui s'étend sur un rectangle et qui s'annule sur le reste. Les valeurs propres sont encore une fois de la forme attendue.

\begin{figure}[!ht]
    \centering
    \subfloat[$\lambda = 12.3046843454\approx \pi^2+ \frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk6_1}}
    \subfloat[$\lambda = 19.7193795458 \approx 2\pi^2$]{\includegraphics[width=.45\textwidth]{figures/tsk6_2}}\\
    \subfloat[$\lambda = 19.7193795458 \approx 2\pi^2$]{\includegraphics[width=.45\textwidth]{figures/tsk6_3}}
    \subfloat[$\lambda = 32.0604871068 \approx \pi^2+9\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk6_4}}\\
    \subfloat[$\lambda = 41.8157840965\approx 4\pi^2+\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk6_5}}
    \subfloat[$\lambda = 49.2981828300\approx 5\pi^2$]{\includegraphics[width=.45\textwidth]{figures/tsk6_6}}
    \caption{Solutions pour différents modes}
\end{figure}

\begin{figure}[!ht]
    \centering
    \subfloat[$\lambda = 49.2981828300\approx 5\pi^2$]{\includegraphics[width=.45\textwidth]{figures/tsk6_7}}
    \subfloat[$\lambda = 49.2377994218\approx 5\pi^2$]{\includegraphics[width=.45\textwidth]{figures/tsk6_8}}\\
    \subfloat[$\lambda = 49.3191384881\approx 5\pi^2$]{\includegraphics[width=.45\textwidth]{figures/tsk6_9}}
    \subfloat[$\lambda = 61.6227271734\approx 4\pi^2 + 9\frac{\pi^2}{4}$]{\includegraphics[width=.45\textwidth]{figures/tsk6_10}}
    \caption{Solutions pour différents modes}
\end{figure}

\FloatBarrier

\section{Résolution de l'équation de la chaleur \---- climatisation d'une pièce isolée}

\subsection*{Tâche 7}

On utilise exactement la même méthode que précédemment. Seulement, on fait une passe supplémentaire pour les nœuds qui apparaissent deux fois à cause des nœuds virtuels.

\subsection*{Tâche 8}



\[
    \begin{aligned}
        \frac{u_h^{(k+1)}-u_h^{(k)}}{\delta t} &= -\nu A_h \left( \theta u_h^{(k+1)} + (1-\theta)u_h^{(k)}\right) + \theta f_h^{(k+1)} + (1-\theta)f_h^{(k)}\\
        u_h^{(k+1)}-u_h^{(k)} &= -\delta t\theta\nu A_h   u_h^{(k+1)} - \nu\delta t(1-\theta) A_hu_h^{(k)} + \delta t f_h
    \end{aligned}
\]
Puisque le second membre est indépendant du temps. Et en appelant $s$ la taille de $A_h$
\[
    (I_s+\nu  \delta t \theta A_h)u_h^{(k+1)} = (I_s- \nu \delta t(1-\theta)A_h)u_h^{(k)} + \delta t f_h
\]

On doit donc résoudre une suite de problème de ce genre pour avoir l'état à un temps donné. Mais on va tenter d'aller plus loin et de dérouler un peu la récurrence.

On peut supposer que $I_s+\nu  \delta t \theta A_h$ est inversible. En effet, son déterminant est un polynôme des coefficients de la matrice. On pose ici la fonction $x\mapsto I_s + xA_h$ qui est une fonction continue de $\RR$ dans $\M_s(\RR)$. Par suite $D:x\mapsto \det(I_s + xA_h)$ est une fonction polynômiale de degré au plus $s$. D'autre part $D(0) = 1$ donc $D$ n'est pas la fonction nulle et s'annule au plus $s$ fois, il est donc possible de trouver un $\delta t$ qui rend $D$ non nul. En particulier, par continuité, en s'approchant de 0, on est certain de rentre $I_s+\nu  \delta t \theta A_h$ inversible.

Par un raisonnement similaire, on peut prouver que $I_s- \nu \delta t(1-\theta)A_h$ peut être rendue inversible.

\[
    u_h^{(k+1)} = (I_s+\nu  \delta t \theta A_h)^{-1}(I_s- \nu \delta t(1-\theta)A_h)u_h^{(k)} + (I_s+\nu  \delta t \theta A_h)^{-1}\delta t f_h
\]

On pose $R = \frac{1}{I_s - (I_s+\nu  \delta t \theta A_h)^{-1}(I_s - \nu \delta t(1-\theta)A_h)}(I_s+\nu  \delta t \theta A_h)^{-1}\delta tf_h$.

Et on trouve $u_h^{(n)} = \left( (I_s+\nu  \delta t \theta A_h)^{-1}(I_s- \nu \delta t(1-\theta)A_h) \right)^n (u_{0,h} - R) + R$.

Il existe des points singuliers mais, dans le cadre de la simulation numérique, ce n'est pas problématique.


Pour $\theta = \frac{1}{2}$, on reconnait le schéma de \textsc{Crank-Nicholson}, si $\theta = 0$, c'est le schéma d'\textsc{Euler} implicite et pour $\theta = 1$, on trouve le schéma d'Euler explicite\cite{goncalvesdasilva:cel-00556967}.

\subsection*{Tâche 9}

\begin{figure}[!ht]
    \centering
    \subfloat[$\theta = 0$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta0}}
    \subfloat[$\theta = 0.25$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta025}}\\
    \subfloat[$\theta = 0.5$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta05}}
    \subfloat[$\theta = 1$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta1}}
    \caption{Températures min, moyennes et max (de bas en haut) pour plusieurs $\theta$}
\end{figure}

\FloatBarrier
On observe des résultats sensiblement identiques, aux erreurs sur les flottants près. D'autre part, cela ne nous renseigne pas sur l'apparition d'un régime permanent. Il suffit de prendre la fonction $\sin$ sur une période pour voir des max, moyennes et min invariant par translation de la fenêtre d'observation, elle n'est pas pour autant constante, ni même convergente.

\FloatBarrier

\begin{figure}[!ht]
    \centering
    \subfloat[$\theta = 0$ et $t=0.025$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta0_2}}
    \subfloat[$\theta = 0$ et $t=0.05$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta0_3}}\\
    \subfloat[$\theta = 0.25$ et $t=0.025$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta025_2}}
    \subfloat[$\theta = 0.25$ et $t=0.05$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta025_3}}
    \caption{Graphe de la température pour plusieurs $\theta$ et à différents temps}
\end{figure}


\begin{figure}[!ht]
    \centering
    \subfloat[$\theta = 0.5$ et $t=0.025$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta05_2}}
    \subfloat[$\theta = 0.5$ et $t=0.05$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta05_3}}\\
    \subfloat[$\theta = 1$ et $t=0.025$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta1_2}}
    \subfloat[$\theta = 1$ et $t=0.05$]{\includegraphics[width=.45\textwidth]{figures/tsk9theta1_3}}
    \caption{Graphe de la température pour plusieurs $\theta$ et à différents temps}
\end{figure}

On n'obtient aucune différence majeure. On remarque un tracé plus régulier et moins chaotique avec $\theta$ proche de 0. Ensuite, après $0.1$s, on atteint un état quasi-stationnaire.

Les résultats sont assez satisfaisant, c'est exactement ce qu'on pouvait attendre d'une pièce rectangulaire homogène et isotrope dont on chauffe un côté et dont on refroidit le côté opposé en supposant que les deux autres sont parfaitement isolés. Ainsi, cette simulation de contrôle confirme le modèle mathématique qui peut servir pour d'autres simulations dont on ne connait pas le résultat.

\bigskip

À propos de l'implémentation. On peut constater la présence de boucle \texttt{for} à plusieurs reprises. Elles ne sont pas là par paresse de faire autrement mais dans un soucis d'optimisation. Lors du tracé des graphes dans les tâches 3 et 6, la boucle ne contient aucun calcul et permet des ajustements fins sur les graphiques, aussi, le temps de la boucle est négligeable devant tous les autres temps. Dans la tâche 9, il y a une boucle qui contient des calculs. Le but est en effet de crée le vecteur $(u^0, u^t, u^{2t}, \ldots, u^{kt})$. Si on laisse Matlab faire, il y a deux inconvénient. D'abord, une utilisation mémoire excessive. Il n'est pas nécessaire d'avoir toutes ces valeurs, il suffit de les énumérer. L'utilisation importante en mémoire ruine la localité et par suite les performances. D'autre part, suite à des tests il semble que Matlab\footnote{version R2013a} est incapable de se rendre compte que chaque composante du vecteur peut être obtenue à partir de la précédente. Aussi, pour paralléliser, il les recalcule toutes depuis le départ, entrainant une perte de temps, même avec les algorithmes d'exponentiation rapide. La boucle le force à calculer chacun en fonction du précédent, ce qui est plus efficace. On aurait pu avoir un gain de performance encore supérieur en autorisant le calcul à partir de plus d'un terme précédent. Le principe de l'exponentiation rapide repose sur 
\[
    \begin{aligned}
        u^{1} &= u\\
        u^{2n} &= {(u^n)}^{2}\\
        u^{2n+1} &= {(u^n)}^{2}\times u \\
    \end{aligned}
\]
Ainsi, en autorisant plus de termes, on aurait un gain certain (calcul en au plus 2 multiplication plutôt que $\log n$). Cependant, ce gain n'est pas si important en pratique à cause des accès mémoire. On aurait pu choisir de garder quelques résultats pour accélérer avec une mémoire pas trop importante mais ça aurait été beaucoup de complication pour gagner un temps assez faible, surtout si on le compare aux autres calculs.

\FloatBarrier

\addcontentsline{toc}{section}{Références}
\bibliographystyle{alpha}
\bibliography{rapport}


\appendix

\section{Sources Matlab}

Les sources qui suivent sont parfaitement fonctionnelles et ont été testées méticuleusement sous Matlab R2013a avant d'être utilisée pour produire les résultats précédents.

On notera le soin pris de prendre des noms de variables et de fonctions très explicites et verbeuses faisant ainsi un code autodocumenté dont la lecture est naturelle sans avoir besoin de remonter sans cesse dans le code pour vérifier le rôle des variables ou de lire des commentaires qui cassent la dynamique et la logique du code.

D'autre part, on constatera la présence de la gestion d'erreur. Ces erreurs, ne devant pas arriver, sont toutefois gérées, ainsi le programme termine sans que Matlab ait besoin de l'arrêter pour cause d'opération illicite.

Comme toujours, notamment en C et sous les systèmes dérivés d'Unix, un code de sorti de 0 signifie que le programme a terminé correctement alors qu'un code différent (ici 1) signale une erreur\footnote{\url{https://en.wikipedia.org/wiki/Exit_status}}.

\subsection{Domaine carré}
\begin{minted}[linenos=true,fontsize=\footnotesize ,bgcolor=bg, tabsize=4]{matlab}
function exitCode=laplacienCarre(N)
    exitCode = 1;
        
    [ret, A] = buildACarre(N);
    
    if(ret ~=0)
        return;
    end
    s = (N-1)*(N-1);
    if(~isequal(size(A), [s, s]))
        disp('ERREUR : A n''a pas la taille attendue.');
        return;
    end
    nbEigenvalues = 10;
    [eigenVectors, experimentalSmallerEigenvalues] = eigs(A, nbEigenvalues, 'sm');
    experimentalSmallerEigenvalues = diag(experimentalSmallerEigenvalues);
    B = sortrows([experimentalSmallerEigenvalues eigenVectors'], 1);
    experimentalSmallerEigenvalues = B(:,1);
    eigenVectors = B(:,2:(N-1)*(N-1)+1)';
    
    theoricSmallerEigenvalues = genTheoriticalSmallerEigenvaluesCarre(nbEigenvalues);
    fprintf(1, 'Valeurs propres approximées :\n');
    fprintf(1, '    %.10f\n', experimentalSmallerEigenvalues);
    fprintf(1, 'Valeurs propres :\n');
    fprintf(1, '    %.10f\n', theoricSmallerEigenvalues);
    fprintf(1, 'Erreurs relatives :\n');
    fprintf(1, '    %.10f\n', abs(experimentalSmallerEigenvalues-theoricSmallerEigenvalues)
                ./theoricSmallerEigenvalues);
        
    fprintf(1, 'Affichage des solutions calculees.\n');
    listSumOfSquares = genListSumOfSquares(nbEigenvalues); 
    for i=1:nbEigenvalues,
        v = eigenVectors(:,i);
        M = [zeros(1,N+1);zeros(N-1,1) reshape(v, [N-1, N-1]) zeros(N-1,1);zeros(1,N+1)]; 
        M = M/max(abs(M(:)));
        figure1 = figure;
        axes1 = axes('Parent', figure1); %#ok<LAXES>
        grid(axes1,'on');
        hold(axes1,'all');
        surf(M, 'EdgeColor', 'None');
        title(['Fonction associée à la valeur propre ' int2str(listSumOfSquares(i)) 'pi^2/4']);
        colorbar('peer',axes1);
    end
    exitCode = 0;
end

function L=genTheoriticalSmallerEigenvaluesCarre(nb)
    L = genListSumOfSquares(nb);
    L = pi^2*L/4;
end

function L=genListSumOfSquares(nb)
    sq = (1:nb).^2;
    [X,Y] = meshgrid(sq);
    L = sort(sum([X(:), Y(:)],2));
    L = L(1:nb);
end

function [exitCode, A]=buildACarre(N)
    [exitCode, A] = buildA(N, (N-1)*(N-1), 
            getNordCarre(N), getSudCarre(N), getEstCarre(N), getOuestCarre(N));
end
\end{minted}

\begin{minted}[linenos=true,fontsize=\footnotesize ,bgcolor=bg, tabsize=4,firstnumber=63]{matlab}
function [exitCode, A]=buildA(N, size, nord, sud, est, ouest)
    exitCode = 1;
    A = 4*speye(size);
    pos = meshgrid(1:size,1);
    if (max(sud) > size || min(sud) < 0)
        disp('ERREUR : sud est mal formé');
        return;
    end
    if (max(nord) > size || min(nord) < 0)
        disp('ERREUR : nord est mal formé');
        return;
    end
    if (max(est) > size || min(est) < 0)
        disp('ERREUR : est est mal formé');
        return;
    end
    if (max(ouest) > size || min(ouest) < 0)
        disp('ERREUR : ouest est mal formé');
        return;
    end
    A = A + sparse(sud(sud>0), pos(sud>0), -ones(size(sud(sud>0))), size, size);
    A = A + sparse(nord(nord>0), pos(nord>0), -ones(size(nord(nord>0))), size, size);
    A = A + sparse(est(est>0), pos(est>0), -ones(size(est(est>0))), size, size);
    A = A + sparse(ouest(ouest>0), pos(ouest>0), -ones(size(ouest(ouest>0))), size, size);
    h = 2/N;
    A = A/(h^2);
    exitCode = 0;
end

function est=getEstCarre(N)
    X = meshgrid(1:(N-1)*(N-1),1);
    est = X'+N-1;
    est(est > (N-1)*(N-1)) = 0;
end

function ouest=getOuestCarre(N)
    X = meshgrid(1:(N-1)*(N-1),1);
    ouest = X'-N+1;
    ouest(ouest <= 0) = 0;
end

function nord=getNordCarre(N)
    X = meshgrid(1:(N-1)*(N-1),1);
    nord = X'-1;
    nord(mod(nord, N-1) == 0) = 0;
end

function sud=getSudCarre(N)
    X = meshgrid(1:(N-1)*(N-1),1);
    sud = X'+1;
    sud(mod(sud, N-1) == 1) = 0;
end
\end{minted}


\subsection{Domaine en L}
\begin{minted}[linenos=true,fontsize=\footnotesize ,bgcolor=bg, tabsize=4]{matlab}
function exitCode=laplacienL(N)
    exitCode = 1;
    M = floor((N-1)/2);
        
    [ret, A] = buildAL(N);
    
    if(ret ~=0)
        return;
    end
    s = (N-1)*M+(N-1-M)*M;
    if(~isequal(size(A), [s, s]))
        disp('ERREUR : A n''a pas la taille attendue.');
        return;
    end
    
    nbEigenvalues = 10;
    [eigenVectors, experimentalSmallerEigenvalues] = eigs(A, nbEigenvalues, 'sm');
    experimentalSmallerEigenvalues = diag(experimentalSmallerEigenvalues);
    B = sortrows([experimentalSmallerEigenvalues eigenVectors'], 1);
    experimentalSmallerEigenvalues = B(:,1);
    eigenVectors = B(:,2:(N-1)*M+(N-1-M)*M+1)';
    
    fprintf(1, 'Valeurs propres approximées :\n');
    fprintf(1, '    %.10f\n', experimentalSmallerEigenvalues);
        
    fprintf(1, 'Affichage des solutions calculées.\n');
    for i=1:nbEigenvalues,
        v = eigenVectors(:,i);
        m = [zeros(N-1-M,N-1-M); reshape(v((N-1)*M+1:(N-1)*M+(N-1-M)*M),[M,N-1-M])];
        m = [reshape(v(1:(N-1)*M),[N-1,M]) m ]; %#ok<AGROW>
        m = [zeros(1,N+1);zeros(N-1,1) m zeros(N-1,1);zeros(1,N+1)]; %#ok<AGROW>
        m = rot90(m/max(abs(m(:))),3);
        figure1 = figure;
        axes1 = axes('Parent', figure1); %#ok<LAXES>
        grid(axes1,'on');
        hold(axes1,'all');
        surf(m, 'EdgeColor', 'None');
        title(['Fonction associée à la valeur propre ' 
                num2str(experimentalSmallerEigenvalues(i))]);
        colorbar('peer',axes1);
    end
    exitCode = 0;
end

function est=getEstL(N)
    function est=getEstL1(N)
        M = floor((N-1)/2);
        X = meshgrid(1:(N-1)*(M-1),1);
        est = X' + N-1;
        est(est > (N-1)*M) = 0;
    end
    function est = getEstL2(N)
        M = floor((N-1)/2);
        X = meshgrid(1:(N-1)*M+N-1,1);
        est = X' - (N-1-M)+(N-1)*M;
        est(est < M*(N-1)+1) = 0;
        est(est > M*(N-1) + (N-1-M)*M) = 0;
    end
    est = getEstL1(N);
    est = [est; getEstL2(N)];
end
\end{minted}

\begin{minted}[linenos=true,fontsize=\footnotesize ,bgcolor=bg, tabsize=4, firstnumber=62]{matlab}
function ouest=getOuestL(N)
    function ouest=getOuestL1(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1)+M,1);
        ouest = X'-N+1;
        ouest(ouest <= 0) = 0;
    end
    function ouest = getOuestL2(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1-M-1),1);
        ouest = X' + (N-1)*M+M - M;
    end
    ouest = getOuestL1(N);
    ouest = [ouest; getOuestL2(N)];
end

function nord=getNordL(N)
    function nord = getNordL1(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1),1);
        nord = X'-1;
        nord(mod(nord, N-1) == 0) = 0;
    end
    function nord = getNordL2(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1-M),1);
        nord = X' + (N-1)*M - 1;
        nord(mod(nord, M) == 0) = 0;
    end
    nord = getNordL1(N);
    nord = [nord; getNordL2(N)];
end

function sud=getSudL(N)
    function sud = getSudL1(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1),1);
        sud = X'+1;
        sud(mod(sud, N-1) == 1) = 0;
    end
    function sud = getSudL2(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1-M),1);
        sud = X' + (N-1)*M + 1;
        sud(mod(sud, M) == 1) = 0;
    end
    sud = getSudL1(N);
    sud = [sud; getSudL2(N)];
end

function [exitCode, A]=buildAL(N)
    M = floor((N-1)/2);
    [exitCode, A] = 
            buildA(N, (N-1)*M+(N-1-M)*M, getNordL(N), getSudL(N), getEstL(N), getOuestL(N));
end
\end{minted}


\subsection{Résolution de l'équation de la chaleur}
\begin{minted}[linenos=true,fontsize=\footnotesize ,bgcolor=bg, tabsize=4]{matlab}
function exitCode=chaleur(N)
    exitCode = 1;
    if(N <= 4)
        disp('ERREUR : choisissez un N>5. Les cas N<=4 sont pathologiques.');
        return;
    end
    nu = 10;
    theta = 1;
    T = 10;
    step = 0;
    stepMax = 101;
    s = 2*(N-1)*(N-1);
    [ret, A, f] = buildAChaleur(N, 28.5*ones(1,N-1), -28.5*ones(1,N-1), zeros(1, 2*(N-2)), nu);
    if(ret ~=0)
        return;
    end
    if(~isequal(size(A), [s, s]) || ~isequal(size(f), [s, 1]))
        disp('ERREUR : A ou f n''a pas la taille attendue.');
        return;
    end
    
    dt = 0.001;
    if (theta < 0.5)
        maxEigenValue = eigs(A,1);
    
        if(maxEigenValue <= 0)
            disp('ERREUR : A semble ne pas avoir de valeur propre strictement positive.');
            return;
        end
        dt = min(0.001, 0.05/((1-2*theta)*nu*maxEigenValue));
    end
    B = speye(s) + nu*dt*theta*A;
    if(det(B) == 0)
        disp('ERREUR : choisissez un pas de temps plus petit.');
        return;
    end
    C = speye(s)-nu*dt*(1-theta)*A;
    R = (speye(s)-B^(-1)*C)^(-1)*B^(-1)*dt*f;
    u = 290*ones(s,1);
    sup = [];
    inf = [];
    moy = [];
    steps = [];
    for i=1:stepMax,
        u = R + full((B^(-1)*C))^(step)*(u-R);
        sup = [sup max(u)]; %#ok<AGROW>
        moy = [moy mean(u)]; %#ok<AGROW>
        inf = [inf min(u)]; %#ok<AGROW>
        steps = [steps step*dt]; %#ok<AGROW>
        axis([1 (N-1) 1 2*(N-1)])
        step = step + T/(dt*(stepMax-1));
    end
    plot(steps,sup,'color','r'); hold on;
    plot(steps,moy,'color','b'); hold on;
    plot(steps,inf,'color','g');
    exitCode = 0;
end
\end{minted}

\begin{minted}[linenos=true,fontsize=\footnotesize ,bgcolor=bg, tabsize=4, firstnumber=58]{matlab}
function [exitCode, A,f]=buildAChaleur(N, g1, g2, g3, nu)
    %Construction de A
    exitCode = 1;
    est = getEstChaleur(N);
    ouest = getOuestChaleur(N);
    nord = getNordChaleur(N);
    sud = getSudChaleur(N);
    s = (N-1)*(N-1)*2;
    A = 4*speye(s);
    pos = meshgrid(1:s,1);
    
    if (max(sud) > s || min(sud) < 0)
        disp('ERREUR : sud est mal formé');
        return;
    end
    if (max(nord) > s || min(nord) < 0)
        disp('ERREUR : nord est mal formé');
        return;
    end
    if (max(est) > s || min(est) < 0)
        disp('ERREUR : est est mal formé');
        return;
    end
    if (max(ouest) > s || min(ouest) < 0)
        disp('ERREUR : ouest est mal formé');
        return;
    end
    
    A = A + sparse(sud(sud>0), pos(sud>0), -ones(size(sud(sud>0))), s, s);
    A = A + sparse(nord(nord>0), pos(nord>0), -ones(size(nord(nord>0))), s, s);
    A = A + sparse(est(est>0), pos(est>0), -ones(size(est(est>0))), s, s);
    A = A + sparse(ouest(ouest>0), pos(ouest>0), -ones(size(ouest(ouest>0))), s, s);

    haut = pos(mod(pos, 2*(N-1)) == 1);
    if (~isequal(size(haut), [1, N-1]) || min(haut) < 1 || max (haut) > s )
        disp('ERREUR : haut est mal formé');
        return;
    end
    A = A + sparse(haut, haut+1, -ones(1, N-1), s, s);
    
    bas = pos(mod(pos, 2*(N-1)) == 0);
    if (~isequal(size(bas), [1, N-1]) || min(bas) < 1 || max (bas) > s )
        disp('ERREUR : bas est mal formé');
        return;
    end
    A = A + sparse(bas, bas-1, -ones(1, N-1), s, s);
    
    droite = pos(pos > (N-1)*(N-1)*2-2*(N-1));
    if (~isequal(size(droite), [1, (N-1)*2]) || min(droite) < 1 || max (droite) > s )
        disp('ERREUR : droite est mal formé');
        return;
    end
    A = A + sparse(droite, droite-2*(N-1), -ones(1,(N-1)*2), s, s);
 
    gauche = pos(pos <= 2*(N-1));
    if (~isequal(size(gauche), [1, (N-1)*2]) || min(gauche) < 1 || max (gauche) > s )
        disp('ERREUR : gauche est mal formé');
        return;
    end
    A = A + sparse(gauche, gauche+2*(N-1), -ones(1,(N-1)*2), s, s);
    h = 2/N;
    A = A/(h^2);
\end{minted}

\begin{minted}[linenos=true,fontsize=\footnotesize ,bgcolor=bg, tabsize=4, firstnumber=120]{matlab}
    %Construction de f
  
    if(~isequal(size(g1), [1, N-1]))
        disp('ERREUR : g1 n''a pas la bonne taille.');
        return;
    end    
    if(~isequal(size(g2), [1,N-1]))
        disp('ERREUR : g2 n''a pas la bonne taille.');
        return;
    end    
    if(~isequal(size(g3), [1,2*(N-2)]))
        disp('ERREUR : g3 n''a pas la bonne taille.');
        return;
    end    
        
    gauche = gauche(2:2*(N-1)-1);
    droite = droite(2:2*(N-1)-1);
    f = sparse(haut, ones(size(haut)), (2*nu/h)*g1, s, 1);
    f = f + sparse(bas, ones(size(bas)), (2*nu/h)*g2, s, 1);
    f = f + sparse(gauche, ones(size(gauche)), (2*nu/h)*g3, s, 1);
    f = f + sparse(droite, ones(size(droite)), (2*nu/h)*g3, s, 1);
    exitCode = 0;
end

function est=getEstChaleur(N)
    X = meshgrid(1:(N-1)*(N-1)*2,1);
    est = X'+2*(N-1);
    est(est > 2*(N-1)*(N-1)) = 0;
end

function ouest=getOuestChaleur(N)
    X = meshgrid(1:(N-1)*(N-1)*2,1);
    ouest = X'-2*(N-1);
    ouest(ouest <= 0) = 0;
end

function nord=getNordChaleur(N)
    X = meshgrid(1:(N-1)*(N-1)*2,1);
    nord = X'-1;
    nord(mod(nord, 2*(N-1)) == 0) = 0;
end

function sud=getSudChaleur(N)
    X = meshgrid(1:(N-1)*(N-1)*2,1);
    sud = X'+1;
    sud(mod(sud, 2*(N-1)) == 1) = 0;
end
\end{minted}
\end{document}


