function exitCode=laplacien(N)
    exitCode = 0;
    exitCode = exitCode + chaleur(N);
    %exitCode = exitCode + laplacienCarre(N);
    %exitCode = exitCode + laplacienL(N);
end



function exitCode=chaleur(N)
    exitCode = 1;
    if(N <= 4)
        disp('ERREUR : choisissez un N>5. Les cas N<=4 sont pathologiques.');
        return;
    end
    nu = 10;
    theta = 1;
    T = 10;
    step = 0;
    stepMax = 101;
    s = 2*(N-1)*(N-1);
    [ret, A, f] = buildAChaleur(N, 28.5*ones(1,N-1), -28.5*ones(1,N-1), zeros(1, 2*(N-2)), nu);
    if(ret ~=0)
        return;
    end
    if(~isequal(size(A), [s, s]) || ~isequal(size(f), [s, 1]))
        disp('ERREUR : A ou f n''a pas la taille attendue.');
        return;
    end
    
    dt = 0.001;
    if (theta < 0.5)
        maxEigenValue = eigs(A,1);
    
        if(maxEigenValue <= 0)
            disp('ERREUR : A semble ne pas avoir de valeur propre strictement positive.');
            return;
        end
        dt = min(0.001, 0.05/((1-2*theta)*nu*maxEigenValue));
    end
    B = speye(s) + nu*dt*theta*A;
    if(det(B) == 0)
        disp(det(B));
        disp('ERREUR : choisissez un pas de temps plus petit.');
        return;
    end
    C = speye(s)-nu*dt*(1-theta)*A;
    R = (speye(s)-B^(-1)*C)^(-1)*B^(-1)*dt*f;
    u = 290*ones(s,1);
    sup = [];
    inf = [];
    moy = [];
    steps = [];
    for i=1:stepMax,
        u = R + full((B^(-1)*C))^(step)*(u-R);
        %figure1 = figure;
        %axes1 = axes('Parent', figure1); %#ok<LAXES>
        %grid(axes1,'on');
        %hold(axes1,'all');
        %surf(reshape(real(u),2*(N-1),(N-1)), 'EdgeColor', 'None');
        %title(['�tat au temps ' num2str(step*dt)]);
        %colorbar('peer',axes1);
        %view(2);
        sup = [sup max(u)]; %#ok<AGROW>
        moy = [moy mean(u)]; %#ok<AGROW>
        inf = [inf min(u)]; %#ok<AGROW>
        steps = [steps step*dt]; %#ok<AGROW>
        axis([1 (N-1) 1 2*(N-1)])
        step = step + T/(dt*(stepMax-1));
    end
    plot(steps,sup,'color','r'); hold on;
    plot(steps,moy,'color','b'); hold on;
    plot(steps,inf,'color','g');
    exitCode = 0;
end

function [exitCode, A,f]=buildAChaleur(N, g1, g2, g3, nu)
    %Construction de A
    exitCode = 1;
    est = getEstChaleur(N);
    ouest = getOuestChaleur(N);
    nord = getNordChaleur(N);
    sud = getSudChaleur(N);
    s = (N-1)*(N-1)*2;
    A = 4*speye(s);
    pos = meshgrid(1:s,1);
    
    if (max(sud) > s || min(sud) < 0)
        disp('ERREUR : sud est mal form�');
        return;
    end
    if (max(nord) > s || min(nord) < 0)
        disp('ERREUR : nord est mal form�');
        return;
    end
    if (max(est) > s || min(est) < 0)
        disp('ERREUR : est est mal form�');
        return;
    end
    if (max(ouest) > s || min(ouest) < 0)
        disp('ERREUR : ouest est mal form�');
        return;
    end
    
    A = A + sparse(sud(sud>0), pos(sud>0), -ones(size(sud(sud>0))), s, s);
    A = A + sparse(nord(nord>0), pos(nord>0), -ones(size(nord(nord>0))), s, s);
    A = A + sparse(est(est>0), pos(est>0), -ones(size(est(est>0))), s, s);
    A = A + sparse(ouest(ouest>0), pos(ouest>0), -ones(size(ouest(ouest>0))), s, s);

    haut = pos(mod(pos, 2*(N-1)) == 1);
    if (~isequal(size(haut), [1, N-1]) || min(haut) < 1 || max (haut) > s )
        disp('ERREUR : haut est mal form�');
        return;
    end
    A = A + sparse(haut, haut+1, -ones(1, N-1), s, s);
    
    bas = pos(mod(pos, 2*(N-1)) == 0);
    if (~isequal(size(bas), [1, N-1]) || min(bas) < 1 || max (bas) > s )
        disp('ERREUR : bas est mal form�');
        return;
    end
    A = A + sparse(bas, bas-1, -ones(1, N-1), s, s);
    
    droite = pos(pos > (N-1)*(N-1)*2-2*(N-1));
    if (~isequal(size(droite), [1, (N-1)*2]) || min(droite) < 1 || max (droite) > s )
        disp('ERREUR : droite est mal form�');
        return;
    end
    A = A + sparse(droite, droite-2*(N-1), -ones(1,(N-1)*2), s, s);
 
    gauche = pos(pos <= 2*(N-1));
    if (~isequal(size(gauche), [1, (N-1)*2]) || min(gauche) < 1 || max (gauche) > s )
        disp('ERREUR : gauche est mal form�');
        return;
    end
    A = A + sparse(gauche, gauche+2*(N-1), -ones(1,(N-1)*2), s, s);
    h = 2/N;
    A = A/(h^2);
    
    %Construction de f
  
    if(~isequal(size(g1), [1, N-1]))
        disp('ERREUR : g1 n''a pas la bonne taille.');
        return;
    end    
    if(~isequal(size(g2), [1,N-1]))
        disp('ERREUR : g2 n''a pas la bonne taille.');
        return;
    end    
    if(~isequal(size(g3), [1,2*(N-2)]))
        disp('ERREUR : g3 n''a pas la bonne taille.');
        return;
    end    
        
    gauche = gauche(2:2*(N-1)-1);
    droite = droite(2:2*(N-1)-1);
    f = sparse(haut, ones(size(haut)), (2*nu/h)*g1, s, 1);
    f = f + sparse(bas, ones(size(bas)), (2*nu/h)*g2, s, 1);
    f = f + sparse(gauche, ones(size(gauche)), (2*nu/h)*g3, s, 1);
    f = f + sparse(droite, ones(size(droite)), (2*nu/h)*g3, s, 1);
    exitCode = 0;
end

function est=getEstChaleur(N)
    X = meshgrid(1:(N-1)*(N-1)*2,1);
    est = X'+2*(N-1);
    est(est > 2*(N-1)*(N-1)) = 0;
end

function ouest=getOuestChaleur(N)
    X = meshgrid(1:(N-1)*(N-1)*2,1);
    ouest = X'-2*(N-1);
    ouest(ouest <= 0) = 0;
end

function nord=getNordChaleur(N)
    X = meshgrid(1:(N-1)*(N-1)*2,1);
    nord = X'-1;
    nord(mod(nord, 2*(N-1)) == 0) = 0;
end

function sud=getSudChaleur(N)
    X = meshgrid(1:(N-1)*(N-1)*2,1);
    sud = X'+1;
    sud(mod(sud, 2*(N-1)) == 1) = 0;
end



function exitCode=laplacienL(N)  %#ok<DEFNU>
    exitCode = 1;
    M = floor((N-1)/2);
        
    [ret, A] = buildAL(N);
    
    if(ret ~=0)
        return;
    end
    s = (N-1)*M+(N-1-M)*M;
    if(~isequal(size(A), [s, s]))
        disp('ERREUR : A n''a pas la taille attendue.');
        return;
    end
    
    nbEigenvalues = 10;
    [eigenVectors, experimentalSmallerEigenvalues] = eigs(A, nbEigenvalues, 'sm');
    experimentalSmallerEigenvalues = diag(experimentalSmallerEigenvalues);
    B = sortrows([experimentalSmallerEigenvalues eigenVectors'], 1);
    experimentalSmallerEigenvalues = B(:,1);
    eigenVectors = B(:,2:(N-1)*M+(N-1-M)*M+1)';
    
    fprintf(1, 'Valeurs propres approxim�es :\n');
    fprintf(1, '    %.10f\n', experimentalSmallerEigenvalues);
        
    fprintf(1, 'Affichage des solutions calcul�es.\n');
    for i=1:nbEigenvalues,
        v = eigenVectors(:,i);
        m = [zeros(N-1-M,N-1-M); reshape(v((N-1)*M+1:(N-1)*M+(N-1-M)*M),[M,N-1-M])];
        m = [reshape(v(1:(N-1)*M),[N-1,M]) m ]; %#ok<AGROW>
        m = [zeros(1,N+1);zeros(N-1,1) m zeros(N-1,1);zeros(1,N+1)]; %#ok<AGROW>
        m = rot90(m/max(abs(m(:))),3);
        figure1 = figure;
        axes1 = axes('Parent', figure1); %#ok<LAXES>
        grid(axes1,'on');
        hold(axes1,'all');
        surf(real(m), 'EdgeColor', 'None');
        title(['Fonction associ�e � la valeur propre ' num2str(experimentalSmallerEigenvalues(i))]);
        colorbar('peer',axes1);
        %view(2);
    end
    exitCode = 0;
end

function [exitCode, A]=buildAL(N)
    M = floor((N-1)/2);
    [exitCode, A] = buildA(N, (N-1)*M+(N-1-M)*M, getNordL(N), getSudL(N), getEstL(N), getOuestL(N));
end

function est=getEstL(N)
    function est=getEstL1(N)
        M = floor((N-1)/2);
        X = meshgrid(1:(N-1)*(M-1),1);
        est = X' + N-1;
        est(est > (N-1)*M) = 0;
    end
    function est = getEstL2(N)
        M = floor((N-1)/2);
        X = meshgrid(1:(N-1)*M+N-1,1);
        est = X' - (N-1-M)+(N-1)*M;
        est(est < M*(N-1)+1) = 0;
        est(est > M*(N-1) + (N-1-M)*M) = 0;
    end
    est = getEstL1(N);
    est = [est;getEstL2(N)];
end

function ouest=getOuestL(N)
    function ouest=getOuestL1(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1)+M,1);
        ouest = X'-N+1;
        ouest(ouest <= 0) = 0;
    end
    function ouest = getOuestL2(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1-M-1),1);
        ouest = X' + (N-1)*M+M - M;
    end
    ouest = getOuestL1(N);
    ouest = [ouest;getOuestL2(N)];
end

function nord=getNordL(N)
    function nord = getNordL1(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1),1);
        nord = X'-1;
        nord(mod(nord, N-1) == 0) = 0;
    end
    function nord = getNordL2(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1-M),1);
        nord = X' + (N-1)*M - 1;
        nord(mod(nord, M) == 0) = 0;
    end
    nord = getNordL1(N);
    nord = [nord;getNordL2(N)];
end

function sud=getSudL(N)
    function sud = getSudL1(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1),1);
        sud = X'+1;
        sud(mod(sud, N-1) == 1) = 0;
    end
    function sud = getSudL2(N)
        M = floor((N-1)/2);
        X = meshgrid(1:M*(N-1-M),1);
        sud = X' + (N-1)*M + 1;
        sud(mod(sud, M) == 1) = 0;
    end
    sud = getSudL1(N);
    sud = [sud;getSudL2(N)];
end



function exitCode=laplacienCarre(N) %#ok<DEFNU>
    exitCode = 1;
        
    [ret, A] = buildACarre(N);
    
    if(ret ~=0)
        return;
    end
    s = (N-1)*(N-1);
    if(~isequal(size(A), [s, s]))
        disp('ERREUR : A n''a pas la taille attendue.');
        return;
    end
    nbEigenvalues = 10;
    [eigenVectors, experimentalSmallerEigenvalues] = eigs(A, nbEigenvalues, 'sm');
    experimentalSmallerEigenvalues = diag(experimentalSmallerEigenvalues);
    B = sortrows([experimentalSmallerEigenvalues eigenVectors'], 1);
    experimentalSmallerEigenvalues = B(:,1);
    eigenVectors = B(:,2:(N-1)*(N-1)+1)';
    
    theoricSmallerEigenvalues = genTheoriticalSmallerEigenvaluesCarre(nbEigenvalues);
    fprintf(1, 'Valeurs propres approxim�es :\n');
    fprintf(1, '    %.10f\n', experimentalSmallerEigenvalues);
    fprintf(1, 'Valeurs propres :\n');
    fprintf(1, '    %.10f\n', theoricSmallerEigenvalues);
    fprintf(1, 'Erreurs relatives :\n');
    fprintf(1, '    %.10f\n', abs(experimentalSmallerEigenvalues-theoricSmallerEigenvalues)./theoricSmallerEigenvalues);
        
    fprintf(1, 'Affichage des solutions calcul�es.\n');
    listSumOfSquares = genListSumOfSquares(nbEigenvalues); 
    for i=1:nbEigenvalues,
        v = eigenVectors(:,i);
        M = [zeros(1,N+1);zeros(N-1,1) reshape(v, [N-1, N-1]) zeros(N-1,1);zeros(1,N+1)]; 
        M = M/max(abs(M(:)));
        figure1 = figure;
        axes1 = axes('Parent', figure1); %#ok<LAXES>
        grid(axes1,'on');
        hold(axes1,'all');
        surf(M, 'EdgeColor', 'None');
        title(['Fonction associ�e � la valeur propre ' int2str(listSumOfSquares(i)) 'pi�/4']);
        colorbar('peer',axes1);
        view(2);
    end
    exitCode = 0;
end

function L=genTheoriticalSmallerEigenvaluesCarre(nb)
    L = genListSumOfSquares(nb);
    L = pi^2*L/4;
end

function L=genListSumOfSquares(nb)
    sq = (1:nb).^2;
    [X,Y] = meshgrid(sq);
    L = sort(sum([X(:), Y(:)],2));
    L = L(1:nb);
end

function [exitCode, A]=buildACarre(N)
    [exitCode, A] = buildA(N, (N-1)*(N-1), getNordCarre(N), getSudCarre(N), getEstCarre(N), getOuestCarre(N));
end

function [exitCode, A]=buildA(N, s, nord, sud, est, ouest)
    exitCode = 1;
    A = 4*speye(s);
    pos = meshgrid(1:s,1);
    if (max(sud) > s || min(sud) < 0)
        disp('ERREUR : sud est mal form�');
        return;
    end
    if (max(nord) > s || min(nord) < 0)
        disp('ERREUR : nord est mal form�');
        return;
    end
    if (max(est) > s || min(est) < 0)
        disp('ERREUR : est est mal form�');
        return;
    end
    if (max(ouest) > s || min(ouest) < 0)
        disp('ERREUR : ouest est mal form�');
        return;
    end
    A = A + sparse(sud(sud>0), pos(sud>0), -ones(size(sud(sud>0))), s, s);
    A = A + sparse(nord(nord>0), pos(nord>0), -ones(size(nord(nord>0))), s, s);
    A = A + sparse(est(est>0), pos(est>0), -ones(size(est(est>0))), s, s);
    A = A + sparse(ouest(ouest>0), pos(ouest>0), -ones(size(ouest(ouest>0))), s, s);
    h = 2/N;
    A = A/(h^2);
    exitCode = 0;
end

function est=getEstCarre(N)
    X = meshgrid(1:(N-1)*(N-1),1);
    est = X'+N-1;
    est(est > (N-1)*(N-1)) = 0;
end

function ouest=getOuestCarre(N)
    X = meshgrid(1:(N-1)*(N-1),1);
    ouest = X'-N+1;
    ouest(ouest <= 0) = 0;
end

function nord=getNordCarre(N)
    X = meshgrid(1:(N-1)*(N-1),1);
    nord = X'-1;
    nord(mod(nord, N-1) == 0) = 0;
end

function sud=getSudCarre(N)
    X = meshgrid(1:(N-1)*(N-1),1);
    sud = X'+1;
    sud(mod(sud, N-1) == 1) = 0;
end

